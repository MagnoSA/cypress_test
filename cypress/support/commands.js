/// <reference types="cypress" />

import credentials from './credentials'

// COMMANDS - GET  //
Cypress.Commands.add('getEntries', (qs = Cypress.env('CATEGORY_AUTH')) => {
  cy.request({
    method: 'GET',
    url: '/entries',
    headers: credentials.HEADERS,
    failOnStatusCode: false,
    qs: {
      Category: qs
    }
  })
})