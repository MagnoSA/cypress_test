export class LoginPage {

  elements = {
    username : () => cy.get('input[data-test="username"]'),
    password : () => cy.get('input[data-test="password"]'),
    login_button : () => cy.get('input[data-test="login-button"]'),
    sorte_selection : () => cy.get('[data-test="product_sort_container"]')
  }

  login(user, pass) {
    this.elements.username().type(user)
    this.elements.password().type(pass)
    this.elements.login_button().click()
  }


}
