/// <reference types="cypress" />

import { faker } from '@faker-js/faker'
import { LoginPage } from './pages/ui_pages'

const loginPage = new LoginPage()

const userFaker = {
  CREDENTIALS: {
    user: faker.person.fullName(),
    pass: faker.internet.password(),
  }
}

describe('UI Tests', () => {
  beforeEach(() => {
    cy.visit('https://www.saucedemo.com/')
  })
    
  it('Login to the Saucedemo site and validate that the items are sorted by Name (A - Z)', () => {
    loginPage.login(Cypress.env('USER'), Cypress.env('PASS'))
    loginPage.elements.sorte_selection().should('have.value', 'az')
  })

  it('Login to the Saucedemo site and change the order that the items are sorted by Name (Z - A)', () => {
    loginPage.login(Cypress.env('USER'), Cypress.env('PASS'))
    loginPage.elements.sorte_selection().select('za')
    loginPage.elements.sorte_selection().should('have.value', 'za')
  })

  it('Login to the Saucedemo site with invalid user', () => {
    loginPage.login(userFaker.CREDENTIALS.user, Cypress.env('PASS'))
    cy.contains('Username and password do not match any user in this service')
  })

  it('Login to the Saucedemo site with invalid password', () => {
    loginPage.login(Cypress.env('USER'), userFaker.CREDENTIALS.pass)
    cy.contains('Username and password do not match any user in this service')
  })
})
