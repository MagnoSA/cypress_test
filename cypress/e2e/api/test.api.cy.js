/// <reference types="cypress" />

const httpStatus = require('http-status-codes')

describe('API tests', () => {
  it('GET entries and validate all objects with property "Category: Authentication & Authorization"', () => {
    cy.getEntries().then(
      (response) => {
        expect(response.status).to.eq(httpStatus.StatusCodes.OK)
        expect(response.body.count).not.null
        expect(response.body.count).to.be.eql(7)
        for (let i = 0; i < response.body.count; i++) {
          expect(response.body.entries[i].Category).to.be.eql('Authentication & Authorization')
        }
      }
    )}
  )}
)
