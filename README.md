[![pipeline status](https://gitlab.com/MagnoSA/cypress_test/badges/main/pipeline.svg)](https://gitlab.com/MagnoSA/cypress_test/-/commits/main)

# CYPRESS-TESTS

Repository with automated tests with **[Cypress](https://www.cypress.io)** for **API** and **UI**.


**NOTE:** The programming language chosen was JavaScript and the OS Windows.

> The **Cypress** enables reliable end-to-end testing for modern web apps. [GET TO KNOW CYPRESS](https://www.cypress.io)

-----------------------

## Context table

> Index `README`.

  - [Prerequisites](#prerequisites)
  - [Configuration](#configuration)
  - [Installation](#installation)
  - [How to test](#how-to-test)
    - [Tips](#tips)
  - [Report](#report)
  - [Support](#support)

-----------------------

### Prerequisites

- [Allure](https://docs.qameta.io/allure)
- [Cypress](https://docs.cypress.io/guides/overview/why-cypress)
- [NodeJS +18](https://nodejs.org/pt-br/download/package-manager/)
- [VSCode or other IDE](https://code.visualstudio.com/download)

**NOTE:** This project run with Node + 18.

-----------------------

### Configuration

> To clone the project:

```bash
$ git clone https://github.com/MagnoSA/cypress_test.git
```

**IMPORTANT!:** The project simulates the use of *sensitive data* in the _cypress.env.json_ file.

This file was ignored (.gitignore). Clone the project and remove the period (*.*) at the beginning of the `.cypress.env.json` file to run the project.

### Installation

> To install all dependencies via **package.json**:

```js
$ cd /cypress_test
$ npm run clean && npm ci
```
-----------------------

### How to test

After confirming the previous settings, follow the steps below:

> To run interactive mode on Cypress:

```js
$ npm run cy:open
```

> To run all API tests:

```js
$ npm run cy:api
```

> To run all UI tests:

```js
$ npm run cy:web
```

-----------------------

#### Tips

Commands for the project, follow the steps below:

> To remove logs, reports, videos and other things in the project:

```js
$ npm run clean
```

> To use lint in the project:

```js
$ npm run lint
```

> To run the tests and generate a report at the end (need Allure installed):

```js
$ npm test
```

-----------------------

### Report

After testing, follow the steps below to generate reports:

> To generate report:

```js
$ npm run allure:report
```

> To open report in the local server:

```js
$ npm run allure:serve
```

-----------------------

### Support

- Linkedin: <a href="https://www.linkedin.com/in/rodrigo-azevedo-ab36737a/" target="_blank">**Rodrigo Magno**</a>
