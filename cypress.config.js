const allureWriter = require('@shelex/cypress-allure-plugin/writer');

module.exports = {
  video: false,
  defaultCommandTimeout: 30000,
  setTimeout: 30000,
  e2e: {
    baseUrl: 'https://api.publicapis.org',
    chromeWebSecurity: false,
    setupNodeEvents(on, config) {
      allureWriter(on, config);
      return config;
    },
  },
  env: {
    allureResultsPath: 'allure-results',
    allureLogCypress: true,
  }
};
